<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


/**
* Define type of server
*
* Depending on the type other stuff can be configured
* Note: Define them all, don't skip one if other is already defined
*/

define( 'DB_CREDENTIALS_PATH', dirname( ABSPATH ) ); // cache it for multiple use
define( 'WP_LOCAL_SERVER', file_exists( dirname( __FILE__ ) . '/local-config.php' ) );
define( 'WP_STAGING_SERVER', file_exists( dirname( __FILE__ ) . '/staging-config.php' ) );
define( 'WP_LIVE_SERVER', file_exists( dirname( __FILE__ ) . '/production-config.php' ) );

/**
* Load DB credentials
*/

if ( WP_LOCAL_SERVER )
    require dirname( __FILE__ ) . '/local-config.php';
elseif ( WP_STAGING_SERVER )
    require dirname( __FILE__ ) . '/staging-config.php';
elseif ( WP_LIVE_SERVER )
	require dirname( __FILE__ ) . '/production-config.php';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
